import io
import os
from typing import List
import telegram
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    CallbackContext,
)
from loguru import logger

from .worker import Worker
from .img_create import img_create

MSG_EXAMPLE = "Physics\nB19-03\n#eb3434"

logger.add("logs/{time}.log")
worker = Worker()


def start(_update: telegram.Update, _context: CallbackContext) -> None:
    pass


def help_command(update: telegram.Update, _context: CallbackContext) -> None:
    update.message.reply_text(
        f"""To generate image just send message of this format:
<Course name>
<Group name>
<color in hex format> (use https://htmlcolorcodes.com/color-picker/)

Example:
_{MSG_EXAMPLE}_
""",
        parse_mode="markdown",
        disable_web_page_preview=True,
    )


#  Format: Physics B19-03 #eb3434
def msg_process(update: telegram.Update, context: CallbackContext) -> None:
    if update.message is None:
        return

    text = update.message.text.strip().split("\n")
    if len(text) != 3:
        update.message.reply_text(
            "*Incorrect format.* Example:\n" + MSG_EXAMPLE, parse_mode="markdown"
        )
        return

    course_name, group_name, color = text

    if len(color) != 0 and color[0] == "#":
        color = color[1:]
    try:
        color_num = int(color, 16)
    except ValueError:
        color = ""

    if len(color) != 6:
        update.message.reply_text(
            "*Incorrect color format.* Example:\n" + MSG_EXAMPLE, parse_mode="markdown"
        )
        return

    worker.append(
        task_process,
        update,
        context,
        course_name,
        group_name,
        [(color_num >> 16) & 255, (color_num >> 8) & 255, color_num & 255],
    )

    user_id = -1
    if update.effective_user is not None:
        user_id = update.effective_user.id

    logger.debug(f"New task (uid={user_id}): ({course_name}, {group_name}, {color})")

    update.message.reply_text(
        f"""\
*Accepted*
Course name: _{course_name}_
Groub name: _{group_name}_
Background color: _#{color}_

Position in order: *{len(worker)}*""",
        parse_mode="markdown",
    )


def task_process(
    update: telegram.Update,
    _context: CallbackContext,
    course_name: str,
    group_name: str,
    color: List[int],
) -> None:
    img = img_create(course_name, group_name, color)

    img_buf = io.BytesIO()
    img.save(img_buf, format="PNG")
    img_buf.seek(0)
    res_file = telegram.InputFile(img_buf, filename=f"{course_name}_{group_name}.png")
    update.message.reply_document(res_file)


def main() -> None:
    if not os.path.isfile("telegram_token.txt"):
        logger.critical("Telegram token not found, create telegram_token.txt file")
        return

    with open("telegram_token.txt", "r") as f:
        token = f.read().strip()

    worker.start()

    updater = Updater(token, use_context=True)

    dp = updater.dispatcher  # type: ignore

    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, msg_process))

    logger.info("Start polling")

    try:
        updater.start_polling()
        updater.idle()
    except telegram.error.Unauthorized:
        logger.critical("telegram.error.Unauthorized")
    finally:
        logger.info("Stoping")
        worker.need_to_close = True


if __name__ == "__main__":
    main()
