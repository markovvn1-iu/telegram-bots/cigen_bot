import threading
import collections
import time
from typing import Callable, Deque, Tuple, Any
from loguru import logger


class Worker(threading.Thread):
    queue: Deque[Tuple[Callable[..., None], Any, Any]]

    def __init__(self) -> None:
        super().__init__()
        self.need_to_close = False
        self.queue = collections.deque()

    def __len__(self) -> int:
        return len(self.queue)

    def append(self, target: Callable[..., None], *args: Any, **kwargs: Any) -> None:
        self.queue.append((target, args, kwargs))

    def run(self) -> None:
        while not self.need_to_close:
            if self.queue:
                target, args, kwargs = self.queue.popleft()
                logger.catch(target)(*args, **kwargs)
            time.sleep(1)
