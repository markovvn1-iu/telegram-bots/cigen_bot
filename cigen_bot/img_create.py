from typing import Tuple, List
import numpy as np
from PIL import ImageFont, ImageDraw, Image


LOGO_IMG = np.array(Image.open("static/logo.png"))

font_big = ImageFont.truetype("static/Product Sans Bold.ttf", 124)
font_small = ImageFont.truetype("static/Product Sans Bold.ttf", 74)


def print_text(img: Image, pos: Tuple[int, int], text: str, font: ImageFont) -> Image:
    sz = font.getsize(text)
    pos = (pos[0] - sz[0] // 2, pos[1] - sz[1] // 2)

    draw = ImageDraw.Draw(img)
    draw.text(pos, text, font=font, fill=(255, 255, 255))
    return img


def img_create(course_name: str, group_name: str, color: List[int]) -> Image:
    img = np.zeros((640, 640, 3), np.uint32) + np.array(color)

    h = LOGO_IMG.shape[0]
    w = LOGO_IMG.shape[1]
    y0 = 105 - h // 2
    x0 = (img.shape[1] - w) // 2
    roi = img[y0 : y0 + h, x0 : x0 + w, :]
    img[y0 : y0 + h, x0 : x0 + w, :] = (
        roi * (255 - LOGO_IMG[:, :, None]) // 255 + LOGO_IMG[:, :, None]
    )

    img = img.clip(0, 255).astype(np.uint8)

    img = Image.fromarray(img)
    img = print_text(img, (640 // 2, 640 // 2), course_name, font_big)
    img = print_text(img, (640 // 2, 640 // 2 + 200), group_name, font_small)

    return img
