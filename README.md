# Inno course image generator 

Telegram бот, позволяющий генерировать картинки для бесед в телеграмме. Оформлен под тематику IU. 

### Examples
![](.gitlab/img1.png)

### Telegram token

To connect program to bot use telegam bot token:

```bash
echo "<telegram-bot-token>" > telegram_token.txt
```

### Run for development (locally)

```bash
make venv  # create virtual environment and install dependencies
make up  # run telegram bot
```

### Run for production (docker)

```bash
make docker_build  # build docker image
make docker_up  # run docker container
```

